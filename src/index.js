import Koa from 'koa';
import {errorHandler, logger} from './utils';
import itemRouter from './item/itemRouter';
import bodyParser from 'koa-bodyparser';
import WebSocket from 'ws';
import {init} from './core';
import jwt from 'koa-jwt';
import jsonWebToken from 'jsonwebtoken'
import cors from '@koa/cors';
import {jwtConfig, router as authRouter} from './auth';
import Router from 'koa-router';

const app = new Koa();
const server = require('http').createServer(app.callback());
const wss = new WebSocket.Server({server});
init(wss);
app.use(cors());
wss.on('connection', ws => {
    console.log('Client connected!');
    ws.on('message', message => {
        console.log('received: %s', message);
        const { token }  = JSON.parse(message);
        try{
            var decoded = jsonWebToken.verify(token, jwtConfig.secret);
            console.log('decoded: ',decoded);
            ws.user=decoded;

        } catch (err) {
            console.log("jwt error")
            console.error(err);
        }
    });
});

app.use(bodyParser());

let count = 0;
app.use(logger);
app.use(errorHandler);

const prefix = '/api';
// public
const publicApiRouter = new Router({ prefix });
publicApiRouter
  .use('/auth', authRouter.routes());
app
  .use(publicApiRouter.routes())
  .use(publicApiRouter.allowedMethods());

// The header of each request needs Authorization Bearer - this decodes the token
app.use(jwt(jwtConfig));
// app.use(async (ctx,next) => {
//    console.log('Starea ws: ', ctx.state);
//    await next();
// });

const protectedApiRouter = new Router({ prefix });
protectedApiRouter
    .use('/item', itemRouter.routes());
app
    .use(protectedApiRouter.routes())
    .use(protectedApiRouter.allowedMethods());

server.listen(3000);

// any post or put that do not contain valid data : 400 bad request
// 401 unauthorized
// 404 not found
// 409 conflict
