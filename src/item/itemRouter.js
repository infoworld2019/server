import Router from 'koa-router';
import ItemStore from './ItemStore'
import {broadcast} from '../core';

export const router = new Router();

const items = new ItemStore();

router.get('/', async (ctx, next) => {
    const userId = ctx.state.user._id;
    ctx.response.body = await items.find({userId});
    ctx.response.status = 200;
});

router.get('/:id', async (ctx, next) => {
    const id = ctx.params.id;
    const item = await items.findOne(id);
    if(item){
        ctx.response.body = item;
        ctx.response.status = 200;
    }
    else{
        ctx.response.status = 404;
    }
});

router.post('/', async (ctx, next) => {
    const item  = ctx.request.body; //luam item trimis de client
    item.userId=ctx.state.user._id;
    const insertedItem=await items.insert(item);
    ctx.response.body = '{"text":"Item inserted"}';
    ctx.response.status = 200; //online from koa
    broadcast(insertedItem);
});

router.put('/:id', async (ctx, next) => {
    // const item = ctx.request.body;
    // const props = {};
    // //ctx.params.id;
    // await items.update({ _id: item._id }, item);
    // ctx.body = 'The item was updated!';
    // ctx.response.status = 200;
    const item = ctx.request.body;
    const dbItem = await items.findOne(ctx.params.id);
    if(dbItem.version>item.version){
        ctx.response.status = 409;
        ctx.response.body = dbItem;
    }
    else{
        ctx.response.status = 200;
        item.version += 1;
        items.update({_id: ctx.params.id}, item);
        const newItem = await items.findOne(ctx.params.id);
        ctx.response.body = newItem;
        broadcast(newItem);
    }
});
// resource versions - optimistic

router.delete('/:text', async (ctx, next) => {
    // const txt = ctx.params.text;
    // await items.remove({text:txt});
    // ctx.response.body = "Item was successfully deleted";
    // ctx.response.status = 200;
    // broadcast()
    const props  = ctx.request.body;

    await items.remove(props).
    then((result)=>{
        ctx.response.status = 200;
        ctx.response.body = '{"text": "Deleted succesfully"}';
        broadcast([])
    })
        .catch((reject)=>{
            ctx.response.status = 400;
            ctx.response.body = reject;
        })
});

export default router;
