//import Item from './Item' //fara acolade cand e default
import { ValidationError } from '../core'
import {idGenerator} from "../utils";
import Item from "./Item";
import datastore from 'nedb-promise';


const match = (props, item) => {
    const keys = Object.keys(props);
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        if (props[key] !== item[key]) {
            return false;
        }
    }
    return true;
};

export class ItemStore {
    constructor() {
        this.db = datastore({
            filename: 'db/items.json',
            autoload: true
        })
    }


    static ensureValidItem(item) {
        const issues = item.validate();
        if (issues.length > 0){
            throw new ValidationError(issues);
        }
    }

    async insert(item) {
        const it = new Item(item.text);
        ItemStore.ensureValidItem(it);
        it.userId = item.userId;
        return this.db.insert(it);
    }

    findOne = async (_id) => this.db.findOne({ _id });

    find = async (props) => this.db.find(props);

    update = async (props, item) => this.db.update(props, item);

    remove = async (props) => this.db.remove(props);

    count = async (props) => this.db.ccount(props);

}

export default ItemStore;





